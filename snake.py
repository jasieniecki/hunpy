import sys

from curses import curs_set, wrapper, newwin
from curses.textpad import Textbox
from datetime import datetime as dt
from getopt import getopt, GetoptError
from json import dump, load
from math import log
from random import choice, choices, randint
from time import sleep


def pair_add(a, b):
    return a[0] + b[0], a[1] + b[1]


def direction_dimension(a):
    if a[0] == 0:
        return 'h'
    return 'v'


class Persistent:
    def __init__(self, path):
        self.path = path
        self.data = {}
        try:
            file = open(self.path, 'r')
            self.data = load(file)
        except:
            pass

    def save(self):
        file = open(self.path, 'w')
        dump(self.data, file)


class State:
    def __init__(self, w, h, square_characters):
        self.w = w
        self.h = h
        self.snake = [(4, 4), (4, 5), (5, 5)]
        self.direction = (1, 0)
        self.food = {}
        self.walls = []
        self.border_wall = False
        self.grow = False
        self.gameover = False
        self.direction_cooldown = 0
        self.direction_buffer = None
        self.bonus_food_timeout = None
        self.immortality_timeout = 0
        self.slow_down_timeout = 0
        self.speed_up_timeout = 0
        self.random_walls_timeout = None
        self.permanent_slow_down = 0
        self.effect_multiplier = 1
        self.multiplier_timeout = None
        self.foods_to_bonus_min = 3
        self.foods_to_bonus_max = 5
        self.roll_foods_to_bonus()
        self.foods_to_surprise_min = 3
        self.foods_to_surprise_max = 7
        self.roll_foods_to_surprise()
        self.food_eaten = 0
        self.score = 0
        self.speed = 1
        self.last_effects = []
        self.add_food()
        if square_characters:
            self.symbols = {
                    'food': '・',
                    'bonus_food': 'め',
                    'surprise': '？',
                    'snake': 'お',
                    'border': 'い',
                    'background': '　',
                    'wall': 'ロ',
            }
        else:
            self.symbols = {
                    'food': '*',
                    'bonus_food': 'O',
                    'surprise': '?',
                    'snake': '@',
                    'border': ':',
                    'background': ' ',
                    'wall': '#',
            }
    
    def __str__(self):
        retval = ""
        border = self.symbols['border'] if not self.border_wall else self.symbols['wall']
        retval += border * (self.w + 2) + "\n"
        for ih in range(self.h):
            retval += border
            for iw in range(self.w):
                if (iw, ih) in self.walls:
                    retval += self.symbols['wall']
                elif (iw, ih) in self.food and (iw, ih) != self.snake[-1]:
                    kind = self.food[iw, ih]
                    if kind == 'f':
                        retval += self.symbols['food']
                    elif kind == 'b':
                        retval += self.symbols['bonus_food']
                    elif kind == '?':
                        retval += self.symbols['surprise']
                elif (iw, ih) in self.snake:
                    retval += self.symbols['snake']
                else:
                    retval += self.symbols['background']
            retval += border
            retval += '\n'
        retval += border * (self.w + 2) + "\n"
        return retval

    def roll_foods_to_bonus(self):
        self.foods_to_bonus = randint(self.foods_to_bonus_min, self.foods_to_bonus_max)

    def roll_foods_to_surprise(self):
        self.foods_to_surprise = randint(self.foods_to_surprise_min, self.foods_to_surprise_max)

    def draw(self, screen, x_offset, y_offset):
        output = str(self)
        i = 0
        screen.addstr(y_offset, x_offset + 1, ' ' * 72)
        screen.addstr(y_offset, x_offset + 1, 'punkty: {:05}     szybkość: {}'.format(self.score, self.speed + 1))
        screen.addstr(y_offset + 1, x_offset + 1, ' ' * 72)
        if self.immortality_timeout:
            screen.addstr(y_offset + 1, x_offset + 1, 'nieśmiertelność: {:3}'.format(self.immortality_timeout))
        elif self.speed_up_timeout:
            screen.addstr(y_offset + 1, x_offset + 1, 'przyspieszenie czasu: {:3}'.format(self.speed_up_timeout))
        elif self.slow_down_timeout:
            screen.addstr(y_offset + 1, x_offset + 1, 'spowolnienie czasu: {:3}'.format(self.slow_down_timeout))
        elif self.random_walls_timeout:
            screen.addstr(y_offset + 1, x_offset + 1, 'przeszkody: {:3}'.format(self.random_walls_timeout))
        elif self.multiplier_timeout:
            screen.addstr(y_offset + 1, x_offset + 1, 'mnożnik x{}: {:3}'.format(self.effect_multiplier, self.multiplier_timeout))
        for line in output.split('\n'):
            screen.addstr(i + y_offset + 2, x_offset, line + '\n')
            i += 1

    def draw_pause(self, screen, x_offset, y_offset):
        for y in [-2, self.h - 1]:
            screen.addstr(y_offset + y + 4, x_offset, self.symbols['wall'] * (self.w + 2))
        for y in range(-1, self.h - 1):
            line = ''
            for x in range(-1, self.w + 1):
                if (x, y + 1) == self.snake[-1]:
                    line += self.symbols['background']
                else:
                    line += self.symbols['wall']
            screen.addstr(y_offset + y + 4, x_offset, line)

    def step(self):
        # check if food was eaten
        if self.snake[-1] in self.food:
            self.food_eaten += 1
            multiplier = (10 + self.food_eaten // 20) * self.effect_multiplier
            if self.snake[-1] in self.snake[:-1]:
                multiplier *= 3
            kind = self.food[self.snake[-1]]
            del self.food[self.snake[-1]]
            if kind == 'f':
                self.score += 1 * multiplier
                self.grow = True
                self.add_food()
            elif kind == 'b':
                self.score += 5 * multiplier
                self.grow = True
                self.bonus_food_timeout = None
            elif kind == '?':
                self.score += 3 * multiplier
                self.random_effect()
            self.foods_to_bonus -= 1
            self.foods_to_surprise -= 1
            if self.foods_to_bonus == 0:
                self.add_food(kind='b')
                self.roll_foods_to_bonus()
            if self.foods_to_surprise == 0:
                self.add_food(kind='?')
                self.roll_foods_to_surprise()
        # check if collision with own body or a wall
        elif (self.snake[-1] in self.snake[:-1] or self.snake[-1] in self.walls) and not self.immortality_timeout:
            self.gameover = True

        # add the new head to the snake
        new = pair_add(self.direction, self.snake[-1])
        wall_collision = False
        if new[0] < 0:
            new = pair_add(new, (self.w, 0))
            wall_collision = True
        if new[1] < 0:
            new = pair_add(new, (0, self.h))
            wall_collision = True
        if new[0] >= self.w:
            new = (0, new[1])
            wall_collision = True
        if new[1] >= self.h:
            new = (new[0], 0)
            wall_collision = True
        self.snake.append(new)
        if wall_collision and self.border_wall and not self.immortality_timeout:
            self.gameover = True

        # remove old tail, unless growing
        if not self.grow:
            self.snake = self.snake[1:]
        else:
            self.grow = False
        
        # direction change cooldown
        if self.direction_cooldown > 0:
            self.direction_cooldown -= 1
            if self.direction_cooldown == 0 and self.direction_buffer is not None:
                dir = self.direction_buffer
                self.direction_buffer = None
                self.set_direction(dir)

        # bonus food timeout
        if self.bonus_food_timeout is not None:
            self.bonus_food_timeout -= 1
            if self.bonus_food_timeout == 0:
                self.food = {k: self.food[k] for k in self.food if self.food[k] != 'b'}
                self.bonus_food_timeout = None

        # multiplier timeout
        if self.multiplier_timeout is not None:
            self.multiplier_timeout -= 1
            if self.multiplier_timeout == 0:
                self.effect_multiplier = 1
                self.multiplier_timeout = None

        # random walls timeout
        if self.random_walls_timeout is not None:
            self.random_walls_timeout -= 1
            if self.random_walls_timeout == 0:
                self.walls = []
                self.border_wall = False
                self.random_walls_timeout = None

        # various timeouts
        if self.immortality_timeout:
            self.immortality_timeout -= 1
        if self.speed_up_timeout:
            self.speed_up_timeout -= 1
        if self.slow_down_timeout:
            self.slow_down_timeout -= 1

        # increase speed
        factor = 4
        if self.speed_up_timeout:
            factor = 1
        elif self.slow_down_timeout:
            factor = 64

        self.speed = self.food_eaten // factor - self.permanent_slow_down

    def random_effect(self):
        effects = (
            # bad:
            (self.random_walls, 15),
            (self.reverse_the_snake, 15),
            (self.speed_up, 10),
            # good:
            (self.slow_down, 30),
            (self.increment_permanent_slow_down, 10),
            (self.enable_immortality, 25),
            (self.cut_the_snake, 15),
            (self.enable_multiplier, 40),
        )
        effects = tuple(((e, w) if e not in self.last_effects else (e, 0)) for e, w in effects)
        methods = [x[0] for x in effects]
        weights = [x[1] for x in effects]
        drawn = choices(methods, weights)[0]
        self.last_effects.append(drawn)
        self.last_effects = self.last_effects[-3:]
        drawn()

    def enable_multiplier(self):
        mutlipliers = (
            (2, 30),
            (3, 20),
        )
        values = [x[0] for x in mutlipliers]
        weights = [x[1] for x in mutlipliers]
        drawn = choices(values, weights)[0]
        self.effect_multiplier = drawn
        self.multiplier_timeout = (self.w + self.h) * 5 // 2

    def random_walls(self):
        self.immortality_timeout = 10  # to allow for adapting
        self.random_walls_timeout = (self.w + self.h)
        algo = choice(('loose', 'walk', 'border'))
        if algo == 'loose':
            for i in range((self.w + self.h) // 3):
                found = False
                x, y = None, None
                while not found:
                    x = randint(0, self.w - 1)
                    y = randint(0, self.h - 1)
                    if (x, y) not in self.walls:
                        found = True
                self.walls.append((x, y))
        elif algo == 'walk':
            step = choice((1, 1, 2, 6))
            count = choice(range(1, 4))
            for j in range(count):
                available_choices = []
                available_choices.append(choice(((0, 1), (0, -1))))
                available_choices.append(choice(((1, 0), (-1, 0))))
                x, y = self.w // 2, self.h // 2
                for i in range((self.w + self.h)):
                    direction = choice(available_choices)
                    for i in range(step):
                        x, y = pair_add((x, y), direction)
                        self.walls.append((x, y))
        elif algo == 'border':
            self.border_wall = True


    def speed_up(self):
        self.speed_up_timeout = (self.w + self.h) * 3 // 4

    def slow_down(self):
        self.slow_down_timeout = (self.w + self.h) // 2

    def increment_permanent_slow_down(self):
        self.permanent_slow_down += 1

    def reverse_the_snake(self):
        self.snake.reverse()
        self.direction = tuple(x - y for x, y in zip(self.snake[-1], self.snake[-2]))

    def enable_immortality(self):
        self.immortality_timeout = (self.w + self.h) * 3 // 2

    def cut_the_snake(self):
        self.snake = self.snake[len(self.snake) // 2 :]

    def set_direction(self, char):
        dict = {
            'w': (0, -1),
            'KEY_UP': (0, -1),
            's': (0, 1),
            'KEY_DOWN': (0, 1),
            'a': (-1, 0),
            'KEY_LEFT': (-1, 0),
            'd': (1, 0),
            'KEY_RIGHT': (1, 0),
        }
        new_dir = dict[char]
        if direction_dimension(new_dir) != direction_dimension(self.direction):
            if self.direction_cooldown == 0:
                self.direction = dict[char]
                self.direction_cooldown = 1
            else:
                self.direction_buffer = char
    
    def add_food(self, kind='f'):
        found = False
        x, y = None, None
        while not found:
            x = randint(0, self.w - 1)
            y = randint(0, self.h - 1)
            if (x, y) not in self.food.keys():
                found = True
        if kind == '?' or kind not in self.food.values():
            self.food[(x, y)] = kind
        if kind == 'b':
            self.bonus_food_timeout = (self.w + self.h) * 2 // 3


def speed_to_timestep(speed):
    speed = speed if speed > -1 else 0
    velocity = 0.00001
    velocity +=  0.000001 * log(speed + 1)
    return int(1 / velocity)


def print_high_scores(screen, high_scores, x_offset, y_offset, max=5):
    i = 0
    max_len = 0
    for name in high_scores.keys():
        name_len = len(name)
        max_len = name_len if name_len > max_len else max_len
    for who, score in sorted(high_scores.items(), key=lambda x: -x[1])[:max]:
        screen.addstr(i + y_offset, x_offset, '{2:5}   {1:{0}}'.format(max_len, who, high_scores[who]))
        i += 1


def ask_user_for_name(screen, score, high_scores):
    curs_set(1)
    max_len = 16
    prompt = 'Your name: [ '
    screen.clear()
    screen.addstr(1, 2, 'Your score: {}'.format(score))
    screen.addstr(3, 2, prompt + ' ' * max_len + ' ]')
    editwin = newwin(1, max_len + 1, 3, 2 + len(prompt))
    screen.addstr(5, 2, '(press CTRL + G to confirm)')
    high_scores['> your score <'] = score
    print_high_scores(screen, high_scores, 5, 7, 16)
    screen.refresh()
    box = Textbox(editwin)
    box.edit()
    name = box.gather()
    curs_set(0)
    name = name.strip(' ')
    return name


def parse_options(options):
    try:
        opts, args = getopt(options, 'sn:', ['square', 'name='])
    except GetoptError as err:
        return None
    context = {}
    for o, a in opts:
        if o in ('-s', '--square'):
            context['square'] = True
        if o in ('-n', '--name'):
            context['name'] = a
    return context


def main(screen):
    pers = Persistent('.snake')
    quit = False
    paused = False
    last = dt.now()
    timestep = 100000  # in microseconds
    width = 23
    height = 12
    x_offset = 1
    y_offset = 1
    x_offset_hs = 5
    y_offset_hs = 5
    options = parse_options(sys.argv[1:])
    if options is None:
        return
    state = State(width, height, square_characters=options.get('square', False))
    screen.timeout(1)
    frame = 0
    curs_set(0)

    while not quit:
        current = dt.now()
        if (current - last).microseconds >= timestep:
            if not paused:
                state.draw(screen, x_offset, y_offset)
            else:
                state.draw_pause(screen, x_offset, y_offset)
            print_high_scores(screen, pers.data, x_offset_hs, y_offset + height + y_offset_hs)
            screen.refresh()
            last = current
            try:
                char = screen.getkey()
                if not paused:
                    if char == 'l':
                        quit = True
                    elif char in ('w', 's', 'a', 'd', 'KEY_UP', 'KEY_DOWN', 'KEY_LEFT', 'KEY_RIGHT'):
                        state.set_direction(char)
                    elif char == 'p':
                        paused = True
                elif char == 'p':
                    paused = False
            except:
                pass
            if frame % 2 == 0:
                if not paused:
                    state.step()
                if state.gameover:
                    quit = True
                timestep = speed_to_timestep(state.speed)
            frame += 1
        sleep(timestep / 100 / 1000 / 1000)
    if state.score > 0:
        name = options.get('name', None)
        if name is None:
            name = ask_user_for_name(screen, state.score, pers.data.copy())
            screen.clear()
        if name != "":
            last = pers.data.get(name, 0)
            if state.score > last:
                pers.data[name] = state.score

    pers.save()


if __name__ == '__main__':
    wrapper(main)
